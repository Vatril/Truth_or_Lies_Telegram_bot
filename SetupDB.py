import sqlite3

DATABASE = 'telegrambot.db'

db = sqlite3.connect(DATABASE)

cur = db.cursor()

cur.execute("DROP TABLE IF EXISTS strikes;")
cur.execute("CREATE TABLE strikes (user_id TEXT, data TEXT);")
cur.execute("DROP TABLE IF EXISTS games;")
cur.execute("CREATE TABLE games (chat_id TEXT, data TEXT);")
cur.execute("DROP TABLE IF EXISTS conversations;")
cur.execute("CREATE TABLE conversations (chat_id TEXT, reason TEXT, data TEXT);")

db.close()
