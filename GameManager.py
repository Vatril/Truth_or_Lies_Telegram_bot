import json
import sqlite3

DATABASE = 'telegrambot.db'


def create_game(data):
    if data["chat"]["type"] == "group":
        return ""
    else:
        return "Please execute this in a group"


def create_strikes(data):
    if data["chat"]["type"] == "group":
        return "Please use this in direct messages"
    else:
        strikedata = {'state': 0, 'name': None, "strikes": []}
        db = sqlite3.connect(DATABASE)
        cur = db.cursor()
        cur.execute("INSERT INTO conversations VALUES ('{0}', 'strikecreation', '{1}');"
                    .format(data["from"]["id"], json.dumps(strikedata)))
        db.commit()
        db.close()
        return "Welcome to the strike creator\n" \
               "/name -> set the name\n" \
               "/strike -> add a strike\n" \
               "/remove -> remove a strike\n" \
               "/done -> finish strike list"


# DATABASE = 'telegrambot.db'
# db = sqlite3.connect(DATABASE)
# cur = db.cursor()
# db.close()

def strike_builder(data, strikedata):
    ret = ""
    db = sqlite3.connect(DATABASE)
    cur = db.cursor()
    strikedata = json.loads(strikedata)
    if strikedata["state"] == 0:
        if data["text"] == "/name":
            strikedata["state"] = 1
            ret = "Enter a name"
        if data["text"] == "/strike":
            strikedata["state"] = 2
            ret = "Enter a name"
        if data["text"] == "/done":
            strikedata["state"] = -1
            ret = "{0} was saved".format(strikedata["name"])
            cur.execute("DROP conversations WHERE chat_id=='{0}';".format(data["from"]["id"]))
            cur.execute("INSERT INTO conversations VALUES ('{0}', '{1}');"
                        .format(data["from"]["id"], json.dumps(strikedata)))
    if strikedata["state"] == 1:
        strikedata["name"] = data["text"]
        strikedata["state"] = 0
    cur.execute("UPDATE conversations SET data='{0}' WHERE chat_id=='{1}';"
                .format(json.dumps(strikedata), data["from"]["id"]))
    db.commit()
    db.close()
    return ret
