import requests
from flask import Flask, request, json

import MessageBuilder as mb

app = Flask(__name__)
with open("secrets.json") as secrets:
    token = json.load(secrets)["token"]

api = "https://api.telegram.org/bot{0}/".format(token)


@app.route("/")
def index():
    return "online"


@app.route("/truth_and_lies_bot/webhook", methods=['POST'])
def webhook():
    jso = request.get_json(force=True)
    print(jso)
    res = None
    if "message" in jso:
        if "entities" in jso["message"]:
            for ent in jso["message"]["entities"]:
                if ent["type"] == "bot_command":
                    res = mb.handle_command(jso["message"])
                    break
    if "inline_query" in jso:
        res = requests.post('{0}answerInlineQuery'.format(api),
                            data={'inline_query_id': jso['inline_query']['id'], 'cache_time': 2,
                                  'results': mb.inline_build_reply(
                                      jso['inline_query']['query'],
                                      jso['inline_query']['from']['id']
                                  )})
    # if "message" in jso:
    #    res = requests.post('{0}sendMessage'.format(api),
    #                        data={'chat_id': jso['message']['chat']['id'],
    #                              'text': "{1}: {0}".format(jso['message']['text'], jso['message']['from']['id'])})
    elif "callback_query" in jso:
        res = requests.post('{0}answerCallbackQuery'.format(api),
                            data={'callback_query_id': jso['callback_query']['id'], 'show_alert': False,
                                  'text': "What you guessed was {0}".format(jso['callback_query']['data'])})

    if res is not None:
        print(res.content)
    return "serverd"


if __name__ == "__main__":
    app.run(ssl_context=('my.pem', 'my.key'), host="0.0.0.0", port=8443)
