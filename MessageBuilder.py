import json
import requests
import sqlite3

import GameManager as gm
from TelegramBot import api

"""
json.dumps([
                                      {'type': 'article', 'id': 0, 'title': 'Test',
                                       'description': 'a test', 'input_message_content': {'message_text': 'huhu'},
                                       'reply_markup': {'inline_keyboard':
                                                                       [[{'text': 'A', 'callback_data': 'A'},
                                                                         {'text': 'B', 'callback_data': 'B'},
                                                                         {'text': 'C', 'callback_data': 'C'},
                                                                         {'text': 'D', 'callback_data': 'D'}]]}
                                                                  }
                                  ])
"""

DATABASE = 'telegrambot.db'


def inline_build_reply(query, id):
    popups = []
    popup = {'type': 'article', 'id': 0, 'title': 'Not recognised',
             'input_message_content': {'message_text': 'Incorrect formatting'},
             'description': '--t for the truth --f for the lie'}
    parts = list(filter(lambda x: len(x) > 0, query.split("--")))
    reply = ""

    keyboard = []
    for i, part in enumerate(parts):
        if part[0] == "T" or part[0] == "t":
            reply = "{2}{0}: {1}\n".format(i + 1, part[1:].strip(), reply)
            keyboard.append({'text': '{0}'.format(i + 1), 'callback_data': "True"})
        if part[0] == "F" or part[0] == "f":
            reply = "{2}{0}: {1}\n".format(i + 1, part[1:].strip(), reply)
            keyboard.append({'text': '{0}'.format(i + 1), 'callback_data': "False"})

    if len(parts) > 0:
        popups.append({'type': 'article', 'id': 0, 'title': '{0} statements found'.format(len(parts)),
                       'input_message_content': {'message_text': reply},
                       'reply_markup': {'inline_keyboard': [keyboard]}})
        for i, part in enumerate(parts):
            listitem = None
            if part[0] == "T" or part[0] == "t":
                listitem = {'type': 'article', 'id': i + 1, 'title': 'Truth', 'description': part[1:].strip(),
                            'input_message_content': {'message_text': reply},
                            'reply_markup': {'inline_keyboard': [keyboard]}}
            if part[0] == "F" or part[0] == "f":
                listitem = {'type': 'article', 'id': i + 1, 'title': 'Lie', 'description': part[1:].strip(),
                            'input_message_content': {'message_text': reply},
                            'reply_markup': {'inline_keyboard': [keyboard]}}
            popups.append(listitem)
    else:
        popups.append(popup)
    return json.dumps(popups)


def handle_command(data):
    text = "Unknown command use /help for command list"
    handled = False

    db = sqlite3.connect(DATABASE)
    cur = db.cursor()
    conversations = cur.execute("SELECT * FROM conversations;").fetchall()
    db.close()
    for conv in conversations:
        if conv[0] == "{0}".format(data["from"]["id"]):
            if conv[1] == "strikecreation":
                text = gm.strike_builder(data, conv[2])
                handled = True
    if not handled:
        if data["text"] == "/start":
            text = "Welcome to Truth and Lies\n" \
                   "Try /help to see how it works\n" \
                   "have fun!"
        if data["text"] == "/help":
            text = "Here are all commands:\n\n" \
                   "/rules -> explains the game\n" \
                   "/create_game -> create new game\n" \
                   "/create_strikeset -> create new strikeset\n" \
                   "/join_game -> join unstarted new game\n" \
                   "/start_game -> start new game\n" \
                   "/end_game -> cancel game\n" \
                   ""
        if data["text"] == "/rules":
            text = ""
        if data["text"] == "/create_game":
            text = gm.create_game(data)
        if data["text"] == "/create_strikeset":
            text = gm.create_strikes(data)
    return requests.post('{0}sendMessage'.format(api),
                         data={'chat_id': data['chat']['id'],
                               'text': text})
